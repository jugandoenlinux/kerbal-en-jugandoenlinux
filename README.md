# Kerbal en JugandoEnLinux

MODs, banderas e instrucciones desde la web jugandoenlinux.com para el videojuego Kerbal Space Program.


## Como instalar las banderas e insignias de JugandoEnLinux en Kerbal:

Descarga las tres imagenes PNG de este repositorio:

- [JEL_flag_transparent.png](https://gitlab.com/jugandoenlinux/kerbal-en-jugandoenlinux/-/blob/main/JEL_flag_transparent.png)
- [JEL_flag_white.png](https://gitlab.com/jugandoenlinux/kerbal-en-jugandoenlinux/-/blob/main/JEL_flag_white.png)
- [JEL_flag_agency.png](https://gitlab.com/jugandoenlinux/kerbal-en-jugandoenlinux/-/blob/main/JEL_flag_agency.png)

Copialas en las siguiente carpetas del juego:
Para Debian/Ubuntu:
```
~/.steam/debian-installation/steamapps/common/Kerbal Space Program/GameData/Squad/Flags/JEL_flag_transparent.png
~/.steam/debian-installation/steamapps/common/Kerbal Space Program/GameData/Squad/Flags/JEL_flag_white.png
~/.steam/debian-installation/steamapps/common/Kerbal Space Program/GameData/Squad/FlagsAgency/JEL_flag_agency.png
```
Para Arch/Manjaro:
```
~/.steam/steamapps/common/Kerbal Space Program/GameData/Squad/Flags/JEL_flag_transparent.png
~/.steam/steamapps/common/Kerbal Space Program/GameData/Squad/Flags/JEL_flag_white.png
~/.steam/steamapps/common/Kerbal Space Program/GameData/Squad/FlagsAgency/JEL_flag_agency.png
```


## Mapa Delta-V:

El PDF con el mapa Delta-V te ayuda a calcular el esfuerzo necesario para llegar a realizar las maniobras necesarias para alcanzar un determinado punto (y regreso si es el caso):
- [Kerbal_DvMap_A4.pdf](https://gitlab.com/jugandoenlinux/kerbal-en-jugandoenlinux/-/blob/main/Kerbal_DvMap_A4.pdf)


Mas detalles en:
https://forum.kerbalspaceprogram.com/index.php?/topic/87463-173-community-delta-v-map-27/


## MODS visuales y de sonido mas habituales:
- Chatterer (efecto radio):
https://www.curseforge.com/kerbal/ksp-mods/chatterer
https://github.com/Athlonic/Chatterer

- EVE Redux:
https://forum.kerbalspaceprogram.com/index.php?/topic/196411-19-112-eve-redux-performance-enhanced-eve-maintenance-v11151-07112021/

- SVE:
https://forum.kerbalspaceprogram.com/index.php?/topic/143288-ksp-161-stock-visual-enhancements-v141-20-march-2019/

- Module Manager:
https://forum.kerbalspaceprogram.com/index.php?/topic/50533-18x-112x-module-manager-421-august-1st-2021-locked-inside-edition/

----

## Créditos:
Community Delta-V Maps v2.7 - For KSP 1.7.3:

    This work is licensed under CC BY-NC-SA 4.0.

    You are permitted to use, copy and redistribute the work as-is.
    You may remix your own derivatives (edit values, design, etc.) and release them under your own name.
    You may not use the material for any commercial purposes.
    You must use the same license as the original work.
    You must credit the following people when publishing your derivatives in the material:
        JellyCubes (Original concept)
        WAC (Original design)
        CuriousMetaphor (Original out-of-atmosphere numbers)
		Armisael (Additional out-of-atmosphere numbers)
        Kowgan (Design, original in-atmosphere numbers)
        Swashlebucky (Design)
        AlexMoon (Time of flight)
        Official Wiki (Relay Antenna calculations)
